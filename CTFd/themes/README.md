# Customizing the UI.

What you see in the folders here are "themes", basically the folders contain the files used to generate a specific "look and feel". If you're familiar with either Flask (or even Django), you'll find these folders to be quite familiar and you may not need to read further.

Each directory identifies one theme that CTFd can use. There are two subdirectories in each "theme" folder. 

1. static

2. templates

The static folder contains all the static files i.e. css, js, images, etc. each in their own specific folder.

The templates folder contains templates that will be _rendered_ into html by Flask (using Jinja). Basically it's a templating engine, i.e. you're rendering data from python and mixing it with html.

Currently we are using the "teammatrix" theme that is basically copied over from the "core" theme but has some changes in <pre>css/styles.css</pre> and <pre>css/teammatrix.css</pre>

You could follow the teammatrix theme for guidance on how we did it.

CTFd also allows custom pages to be added that can be edited in a running CTFd instance. See the "Pages" tab in the "Admin" pages to edit those. For example, the first page is "index" and you can make changes there if need be.